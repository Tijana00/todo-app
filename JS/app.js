var todoList = [];

var init = () => {
  getItems();
  renderTodo();
}

var getItems = () => {
  todoList = JSON.parse(localStorage.getItem("data")) || [];
}

var setItems = () => {
  var jsonString = JSON.stringify(todoList);
  localStorage.setItem("data", jsonString);
}

var deleteItem = (id) => {
  todoList.splice(id,1);
  renderTodo();
  setItems();
}

var addItem = () => {
  var item = document.getElementById("todo-input").value;
  todoList.push(item);
  document.getElementById("todo-input").value = "";
  renderTodo();
  setItems();
}

var renderTodo = () => {
  var resultString = "";
  for(var i = 0; i < todoList.length; i++){
    resultString += `
      <div class="todo-list-item">
        <div class="todo-list-id">${i+1}</div>
        <div class="todo-description">
          ${todoList[i]}
        </div>
        <button class="todo-delete" onclick="deleteItem(${i})"><i class="fas fa-trash"></i></button>
      </div>
    `;
  }
  document.getElementById('todo-list').innerHTML = resultString;
}
